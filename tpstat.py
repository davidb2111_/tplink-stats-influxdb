#!/usr/bin/env python3
# coding: utf-8

import argparse
import re
import requests
import time
from influxdb_client import InfluxDBClient
from yaml import load

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader
from bs4 import BeautifulSoup

TPlinkStatus = {
    "0": 0,
    "1": 0,
    "2": 10000000,
    "3": 10000000,
    "4": 100000000,
    "5": 100000000,
    "6": 1000000000,
}


class TPstatus:
    def __init__(self, host, user, password):
        self.host = host
        self.user = user
        self.password = password
        self.url = f"http://{host}"
        self.s = requests.Session()

    def login(self):
        data = {"logon": "Login", "username": self.user, "password": self.password}
        headers = {"Referer": f"{self.url}/Logout.htm"}
        self.login_data = self.s.post(
            f"{self.url}/logon.cgi", data=data, headers=headers, timeout=5
        )

    def get_stats(self):
        self.login()
        headers = {
            "Referer": f"{self.url}/",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
            "Upgrade-Insecure-Requests": "1",
        }
        return self.s.get(
            f"{self.url}/PortStatisticsRpm.htm", headers=headers, timeout=6
        )

    def parse_stats(self):
        soup = BeautifulSoup(self.get_stats().text, "html.parser")

        #        if str(self.r) != "<Response [200]>":
        #            sys.exit("ERROR: Login failure - bad credential?")
        pattern = re.compile(r"var (max_port_num) = (.*?);$", re.MULTILINE)
        max_port_num = int(pattern.search(str(soup.script)).group(2))

        script_vars = (
            re.compile(r"var all_info = {\n?(.*?)\n?};$", re.MULTILINE | re.DOTALL)
            .search(str(soup.script))
            .group(1)
        )

        entries = re.split(",?\n+", script_vars)
        edict = {}
        drop2 = re.compile(r"\[(.*),0,0]")
        for entry in entries:
            e2 = re.split(":", entry)
            edict[str(e2[0])] = drop2.search(e2[1]).group(1)
        e3 = re.split(",", edict["state"])
        e4 = re.split(",", edict["link_status"])
        e5 = re.split(",", edict["pkts"])
        pdict = {}
        jlist = []
        for x in range(1, max_port_num + 1):
            # print(x, ((x-1)*4), ((x-1)*4)+1, ((x-1)*4)+2, ((x-1)*4)+3 )
            pdict[x] = {}
            pdict[x]["state"] = e3[x - 1]
            pdict[x]["link_status"] = TPlinkStatus[e4[x - 1]]
            pdict[x]["tx_good_pkt"] = e5[((x - 1) * 4)]
            pdict[x]["tx_bad_pkt"] = e5[((x - 1) * 4) + 1]
            pdict[x]["rx_good_pkt"] = e5[((x - 1) * 4) + 2]
            pdict[x]["rx_bad_pkt"] = e5[((x - 1) * 4) + 3]

            z = {**{"port": x}, **pdict[x]}
            jlist.append(z)

        for dict in jlist:
            for key in dict:
                dict[key] = int(dict[key])
        return jlist


parser = argparse.ArgumentParser(
    description="TP-Link Easy Smart Switch port statistics."
)
parser.add_argument(
    "-c", "--config", required=False, default=".config.yaml", help="config file to load"
)

args = vars(parser.parse_args())
stream = open(args["config"], "r")
config = load(stream, Loader=Loader)

tp = TPstatus(
    host=config["target"]["host"],
    user=config["target"]["username"],
    password=config["target"]["password"],
)
# print(json.dumps(tp.parse_stats()))


measurements = [
    "state",
    "link_status",
    "tx_good_pkt",
    "tx_bad_pkt",
    "rx_good_pkt",
    "rx_bad_pkt",
]
while True:
    with InfluxDBClient(url=config["influxdb"]["url"], org=config["influxdb"]["org"], token=config["influxdb"]["token"]) as client:
        with client.write_api() as write_api:
            for port_data in tp.parse_stats():
                for measurement in measurements:
                    inf = {
                        "measurement": measurement,
                        "tags": {
                            "host": config["target"]["host"],
                            "port": port_data["port"],
                        },
                        "fields": {"value": port_data[measurement]},
                    }

                    try:
                        write_api.write(
                            bucket=config["influxdb"]["bucket"],
                            org=config["influxdb"]["org"],
                            record=inf,
                            write_precision="s",
                        )
                    except Exception as e:
                        print(f"can't save value: {e.message}")
    time.sleep(10)
